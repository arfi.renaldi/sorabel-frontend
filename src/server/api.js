const path = require('path');

const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors());

const port = 2000;

app.get('/api/category/dress/products', (req, res) => {
    res.sendFile(path.join(__dirname, 'mock-data', 'dress-products-mock.json'));
});

app.get('/api/category/:category', (req, res) => {
    const category = req.params.category;
    res.sendFile(path.join(__dirname, 'mock-data', `${category}-category-mock.json`));
});

app.listen(port, () => {
    console.log(`API listening on port ${port}.`);
});
