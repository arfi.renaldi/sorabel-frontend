export default function titlecase(s) {
  if (typeof s !== 'string') return ''
  const lowerCase = s.replace(/-/g, ' ');
  return lowerCase.replace(/\b\w/g, t => {
    return t.toUpperCase();
  })
}
