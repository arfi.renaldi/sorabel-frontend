export default function snakecase(s) {
  if (typeof s !== 'string') return ''
  return s.replace(/\s+/g, '-').toLowerCase();
}
