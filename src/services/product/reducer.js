import { FETCH_CATEGORY_NAME, FETCH_PRODUCT_BY_CATEGORY, FETCH_ALL_PRODUCT, FETCH_PRODUCT_DETAIL, FETCH_PRODUCT_IMAGE_BY_ID } from './types';

const initialState = {
  allProduct: [],
  categoryList: [],
  productList: [],
  productDetail: {},
  productDetailImageList: [],
  productDetailImage: {},
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORY_NAME:
      return {
        ...state,
        categoryList: action.payload
      };
    case FETCH_ALL_PRODUCT:
      return {
        ...state,
        allProduct: action.payload
      };
    case FETCH_PRODUCT_BY_CATEGORY:
      return {
        ...state,
        productList: action.payload
      };
    case FETCH_PRODUCT_DETAIL:
      return {
        ...state,
        productDetail: action.payload,
        productDetailImageList: action.payload.product_image_list,
      };
    case FETCH_PRODUCT_IMAGE_BY_ID:
      return {
        ...state,
        productDetailImage: action.payload
      };
    default:
      return state;
  }
}
