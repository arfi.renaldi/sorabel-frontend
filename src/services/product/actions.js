import { FETCH_CATEGORY_NAME, FETCH_ALL_PRODUCT, FETCH_PRODUCT_BY_CATEGORY, FETCH_PRODUCT_DETAIL, FETCH_PRODUCT_IMAGE_BY_ID } from './types'

export const fetchCategoryName = (name) => async dispatch => {
    const response = await fetch(`http://localhost:2000/api/category/${name}`);
    const category = await response.json();
    return dispatch({
        type: FETCH_CATEGORY_NAME,
        payload: category.category_list
    });
};

export const fetchAllProduct = () => async dispatch => {
    const response = await fetch('http://localhost:2000/api/category/dress/products');
    const product = await response.json();
    return dispatch({
        type: FETCH_ALL_PRODUCT,
        payload: product
    });
};

export const fetchProductByCategory = (category) => async dispatch => {
    const response = await fetch('http://localhost:2000/api/category/dress/products');
    const product = await response.json();
    product.forEach((item, index) => {
        if(item.category_name === category) {
            return dispatch({
                type: FETCH_PRODUCT_BY_CATEGORY,
                payload: item.product_list
            });
        }
    })
};

export const fetchProductDetail = (id) => async dispatch => {
    const response = await fetch('http://localhost:2000/api/category/dress/products');
    const category = await response.json();
    category.forEach((product) => {
        product.product_list.forEach((item, index) => {
            if(item.product_id === id) {
                return dispatch({
                    type: FETCH_PRODUCT_DETAIL,
                    payload: item
                });
            }
        })
    })
};

export const fetchProductImageById = (productId, imageId) => async dispatch => {
    const response = await fetch('http://localhost:2000/api/category/dress/products');
    const category = await response.json();
    category.forEach(product => {
        product.product_list.forEach(item => {
            if(item.product_id === productId) {
                item.product_image_list.forEach(image => {
                    if(image.product_image_id === imageId) {
                        return dispatch({
                            type: FETCH_PRODUCT_IMAGE_BY_ID,
                            payload: image
                        });
                    }
                })
            }
        })
    })
};

