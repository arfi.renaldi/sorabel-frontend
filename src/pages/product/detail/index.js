import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Breadcrumb, BreadcrumbItem, Col, Row, Nav, NavItem, NavLink, TabContent, TabPane, Table } from 'reactstrap';
import { Button, FormSelect, FormInput, Card, CardBody, CardSubtitle } from 'shards-react';
import classnames from 'classnames';
import { fetchProductDetail, fetchProductImageById } from '../../../services/product/actions';
import './index.css';

class DetailPage extends React.Component {
  static propTypes = {
    fetchProductDetail: PropTypes.func.isRequired,
    productDetail: PropTypes.object,
    productDetailImage: PropTypes.object,
    productDetailImageList: PropTypes.array
  };

  constructor(props) {
    super(props);

    // this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      totalProduct: 1,
    };
  }

  componentDidMount() {
    const {
      params: { id },
    } = this.props.match;
    this.props.fetchProductDetail(parseInt(id));
    this.props.fetchProductImageById(parseInt(id), 1);
  }

  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  changeProductImage = (imageId) => {
    const {
      params: { id },
    } = this.props.match;
    this.props.fetchProductImageById(parseInt(id), parseInt(imageId));
  }

  handleTotalProduct = (e) => {
    const total = e.target.value;
    if(total >= 0) {
      this.setState({
        totalProduct: total
      })
    }
  }

  totalProduct = (e) => {
    const { totalProduct } = this.state;
    const button = e.target.textContent;
    if(button === '+') {
      this.setState({
        totalProduct: totalProduct+1
      })
    } else if(button === '-'){
      if(totalProduct !== 0) {
        this.setState({
          totalProduct: totalProduct-1
        })
      }
    }
  }

  goToDetail = (product) => {
    const { history } = this.props;
    history.push(`/products/${product}`);
  }

  goToList = (category) => {
    const { history, match } = this.props;
    history.push(`${match.url}/${category}`);
  }

  render() {
    const { productDetail, productDetailImage, productDetailImageList } = this.props;
    const { totalProduct } = this.state;

    return (
      <>
        <Breadcrumb>
          <BreadcrumbItem><Link to="/">Products</Link></BreadcrumbItem>
          <BreadcrumbItem><Link to="/">Category</Link></BreadcrumbItem>
          <BreadcrumbItem><Link to="/">Dress</Link></BreadcrumbItem>
          <BreadcrumbItem active>{productDetail.product_name}</BreadcrumbItem>
        </Breadcrumb>
        <div className="product-detail-container">
          <Row>
            <Col md={6} className="product-image">
              <div className="content-img">
                <img style={{ width: '100%' }} alt="" src={productDetailImage.product_image} />
              </div>
              <div className="content-tumbnail">
                {productDetailImageList.map((item, index) => {
                  return(
                    <img style={{width: '100px'}} alt="" onClick={(e) => this.changeProductImage(item.product_image_id)} src={item.product_image} key={item.product_image_id} />
                  )
                })}
              </div>
            </Col>
            <Col md={6} className="product-description">
              <h3>{productDetail.product_name}</h3>
              <h4>{productDetail.product_price}</h4>
              <span>Layanan Produk</span>
              <div className="flex-center">
                <div>
                  <div>Free Ongkir (min 200rb)</div>
                  <div>Bisa COD</div>
                </div>
                <div style={{ marginLeft: '30px' }}>
                  <div>Garansi 30 hari</div>
                  <div>Bisa coba dulu baru bayar</div>
                </div>
              </div>
              {/* <ul>
                <li>Free Ongkir (min 200rb)</li>
                <li>Bisa COD</li>
                <li>Garansi 30 hari</li>
                <li>Bisa coba dulu baru bayar</li>
              </ul> */}
              <div className="size-container flex-center">
                <span className="description-title">Size: </span>
                <FormSelect size="sm">
                  <option value="small">S</option>
                  <option value="medium">M</option>
                  <option value="large">L</option>
                  <option value="xl">XL</option>
                </FormSelect>
              </div>
              <div className="total-container flex-center">
                <span className="description-title">Jumlah: </span>
                <Button className="plus" size="sm" outline theme="secondary" onClick={(e) => this.totalProduct(e)}>-</Button>
                <FormInput size="sm" value={totalProduct} onChange={(e) => this.handleTotalProduct(e)} />
                <Button className="minus" size="sm" outline theme="success" onClick={(e) => this.totalProduct(e)}>+</Button>
              </div>
              <Button theme="success">Beli</Button>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <div className="detail-container">
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={classnames({ active: this.state.activeTab === '1' })}
                      onClick={() => this.toggle('1')}
                    >
                      Detail & Size
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active: this.state.activeTab === '2' })}
                      onClick={() => this.toggle('2')}
                    >
                      Testimoni
                    </NavLink>
                  </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                  <TabPane tabId="1">
                    <Row>
                      <Col sm="12">
                        <div className="detail-description">
                          <span>Detail</span>
                          <br />
                          <span style={{ fontWeight: 'bold' }}>Bahan: </span>
                          <span>Katun Linen</span>
                          <br />
                          <span style={{ fontWeight: 'bold' }}>Detail: </span>
                          <span>Resleting belakang</span>
                        </div>
                        <br />
                        <div className="size-description">
                          <span>Panduan Ukuran</span>
                          <div>
                            <Table responsive="sm" size="md">
                              <thead>
                                <tr>
                                  <th>Size</th>
                                  <th>Lingkar dada</th>
                                  <th>Panjang lengan</th>
                                  <th>Lingkar lengan</th>
                                  <th>Panjang</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>S</td>
                                  <td>100 cm</td>
                                  <td>57 cm</td>
                                  <td>49 cm</td>
                                  <td>129 cm</td>
                                </tr>
                                <tr>
                                  <td>M</td>
                                  <td>104 cm</td>
                                  <td>58 cm</td>
                                  <td>49 cm</td>
                                  <td>130 cm</td>
                                </tr>
                                <tr>
                                  <td>L</td>
                                  <td>110 cm</td>
                                  <td>59 cm</td>
                                  <td>50 cm</td>
                                  <td>131 cm</td>
                                </tr>
                                <tr>
                                  <td>XL</td>
                                  <td>120 cm</td>
                                  <td>60 cm</td>
                                  <td>51 cm</td>
                                  <td>132 cm</td>
                                </tr>
                              </tbody>
                            </Table>
                          </div>
                        </div>
                        <span>Produk Bisa Dicoba dan Dikembalikan</span>
                        <br />
                        <span>Ya</span>
                      </Col>
                    </Row>
                  </TabPane>
                  <TabPane tabId="2">
                    <Row>
                      <Col sm="12">
                        <h4>Testimoni</h4>
                        <Card>
                          <CardBody>
                            <CardSubtitle>John Doe</CardSubtitle>
                            Nunc quis nisl ac justo elementum sagittis in quis justo.
                          </CardBody>
                        </Card>
                        <Card>
                          <CardBody>
                            <CardSubtitle>John Doe</CardSubtitle>
                            Nunc quis nisl ac justo elementum sagittis in quis justo.
                          </CardBody>
                        </Card>
                        <Card>
                          <CardBody>
                            <CardSubtitle>John Doe</CardSubtitle>
                            Nunc quis nisl ac justo elementum sagittis in quis justo.
                          </CardBody>
                        </Card>
                        <Card>
                          <CardBody>
                            <CardSubtitle>John Doe</CardSubtitle>
                            Nunc quis nisl ac justo elementum sagittis in quis justo.
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </TabPane>
                </TabContent>
              </div>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  productDetail: state.product.productDetail,
  productDetailImage: state.product.productDetailImage,
  productDetailImageList: state.product.productDetailImageList,
});

export default connect(
  mapStateToProps,
  { fetchProductDetail, fetchProductImageById }
)(DetailPage);
