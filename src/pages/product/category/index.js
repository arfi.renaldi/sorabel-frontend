import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Accordion } from 'react-bootstrap';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Card, CardBody, CardFooter, CardImg } from 'shards-react';
import { fetchCategoryName, fetchAllProduct } from '../../../services/product/actions';
import snakecase from '../../../helpers/snakecase';
import titlecase from '../../../helpers/titlecase';
import './index.css';

class CategoryPage extends React.Component {
  static propTypes = {
    fetchCategoryName: PropTypes.func.isRequired,
    fetchAllProduct: PropTypes.func.isRequired,
    allProduct: PropTypes.array,
    categoryList: PropTypes.array,
  };

  componentDidMount() {
    this.props.fetchCategoryName('dress');
    this.props.fetchAllProduct();
  }

  goToDetail = (product) => {
    const { history } = this.props;
    history.push(`/products/${product}`);
  }

  goToList = (category) => {
    const { history, match } = this.props;
    history.push(`${match.url}/${category}`);
  }

  renderAllProduct = () => {
    const { allProduct } = this.props;
    return allProduct.map((product, index) => {
      return product.product_list.map((item, index) => {
        return (
          <Card onClick={() => this.goToDetail(`${snakecase(item.product_name)}/${item.product_id}`)} key={item.product_id}>
            <CardImg src={item.product_image} />
            <CardBody>
              <span>{item.product_name}</span>
              <br />
              <span>Rp. {item.product_price}</span>
            </CardBody>
          </Card>
        )
      })
    })
  }

  renderCategoryList = () => {
    const { categoryList } = this.props;
    return categoryList.map((item, index) => {
      return (
        <Card onClick={() => this.goToList(item.category_list_name)} key={item.category_list_id}>
          <CardImg src={item.category_list_image} alt={item.category_list_name} />
          <CardFooter>{titlecase(item.category_list_name)}</CardFooter>
        </Card>
      )
    })
  }

  render() {
    const {
      params: { category },
    } = this.props.match;

    return (
      <>
        <Breadcrumb>
          <BreadcrumbItem><Link to="/">Products</Link></BreadcrumbItem>
          <BreadcrumbItem><Link to="/">Category</Link></BreadcrumbItem>
          <BreadcrumbItem active>{titlecase(category)}</BreadcrumbItem>
        </Breadcrumb>
        <div className="product-category-container">
          <div className="features">
            <Accordion defaultActiveKey="1" style={{ display: 'grid' }}>
              <Accordion.Toggle eventKey="0">
                Blouse
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <ul>
                  <li>Blouse 1</li>
                  <li>Blouse 2</li>
                  <li>Blouse 3</li>
                </ul>
              </Accordion.Collapse>
              <Accordion.Toggle eventKey="1">
                Dress
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="1">
                <ul>
                  <li onClick={() => this.goToList('dress-mini')}>Mini Dress</li>
                  <li onClick={() => this.goToList('dress-midi')}>Midi Dress</li>
                  <li onClick={() => this.goToList('dress-maxi')}>Maxi Dress</li>
                </ul>
              </Accordion.Collapse>
              <Accordion.Toggle eventKey="2">
                Muslim Wear
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="2">
                <ul>
                  <li>Dress Muslim</li>
                  <li>Atasan Muslim</li>
                  <li>Jilbab Muslim</li>
                  <li>Set Muslim</li>
                </ul>
              </Accordion.Collapse>
              <Accordion.Toggle eventKey="3">
                Big Size
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="3">
                <ul>
                  <li>Blouse</li>
                  <li>Kemeja</li>
                  <li>Jaket</li>
                  <li>Outwear</li>
                </ul>
              </Accordion.Collapse>
            </Accordion>
          </div>
          <div className="category">
            <div className="category-list">
              <h3>Kategori</h3>
              <div className="item-list">
                {this.renderCategoryList()}
              </div>
            </div>
            <div className="product-list">
              <h3>Semua Produk</h3>
              <div className="item-list">
                {this.renderAllProduct()}
                {this.renderAllProduct()}
                {this.renderAllProduct()}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  allProduct: state.product.allProduct,
  categoryList: state.product.categoryList,
  productList: state.product.productList
});

export default connect(
  mapStateToProps,
  { fetchCategoryName, fetchAllProduct }
)(CategoryPage);
