import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { Accordion } from 'react-bootstrap';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Card, CardBody, CardImg } from 'shards-react';
import titlecase from '../../../helpers/titlecase';
import snakecase from '../../../helpers/snakecase';
import { fetchProductByCategory } from '../../../services/product/actions';

class ListPage extends React.Component {
  static propTypes = {
    fetchProductByCategory: PropTypes.func.isRequired,
    productList: PropTypes.array
  };

  constructor(props) {
    super(props);

    this.state = {
      categoryTitle: '',
    };
  }

  componentDidMount() {
    const {
      params: { categoryList },
    } = this.props.match;
    this.props.fetchProductByCategory(categoryList);
    this.setState({
      categoryTitle: titlecase(categoryList)
    })
  }

  goToDetail = (product) => {
    const { history } = this.props;
    history.push(`/products/${product}`);
  }

  goToList = (category) => {
    this.props.fetchProductByCategory(category);
    this.setState({
      categoryTitle: titlecase(category)
    })
  }

  renderProductList = () => {
    const { productList } = this.props;
    return productList.map((item, index) => {
      return (
        <Card onClick={() => this.goToDetail(`${snakecase(item.product_name)}/${item.product_id}`)} key={item.product_id}>
          <CardImg src={item.product_image} />
          <CardBody>
            <span>{item.product_name}</span>
            <br />
            <span>Rp. {item.product_price}</span>
          </CardBody>
        </Card>
      )
    })
  }

  render() {
    const { categoryTitle } = this.state;
    const {
      params: { category, categoryList },
    } = this.props.match;

    return (
      <>
        <Breadcrumb>
          <BreadcrumbItem><Link to="/">Products</Link></BreadcrumbItem>
          <BreadcrumbItem><Link to="/">Category</Link></BreadcrumbItem>
          <BreadcrumbItem><Link to="/">{titlecase(category)}</Link></BreadcrumbItem>
          <BreadcrumbItem active>{titlecase(categoryList)}</BreadcrumbItem>
        </Breadcrumb>
        <div className="product-category-container">
          <div className="features">
            <Accordion defaultActiveKey="1" style={{ display: 'grid' }}>
              <Accordion.Toggle eventKey="0">
                Blouse
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <ul>
                  <li>Blouse 1</li>
                  <li>Blouse 2</li>
                  <li>Blouse 3</li>
                </ul>
              </Accordion.Collapse>
              <Accordion.Toggle eventKey="1">
                Dress
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="1">
                <ul>
                  <li onClick={() => this.goToList('dress-mini')} style={categoryTitle === 'Dress Mini' ? {fontWeight: 'bold'} : null}>Mini Dress</li>
                  <li onClick={() => this.goToList('dress-midi')} style={categoryTitle === 'Dress Midi' ? {fontWeight: 'bold'} : null}>Midi Dress</li>
                  <li onClick={() => this.goToList('dress-maxi')} style={categoryTitle === 'Dress Maxi' ? {fontWeight: 'bold'} : null}>Maxi Dress</li>
                </ul>
              </Accordion.Collapse>
              <Accordion.Toggle eventKey="2">
                Muslim Wear
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="2">
                <ul>
                  <li>Dress Muslim</li>
                  <li>Atasan Muslim</li>
                  <li>Jilbab Muslim</li>
                  <li>Set Muslim</li>
                </ul>
              </Accordion.Collapse>
              <Accordion.Toggle eventKey="3">
                Big Size
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="3">
                <ul>
                  <li>Blouse</li>
                  <li>Kemeja</li>
                  <li>Jaket</li>
                  <li>Outwear</li>
                </ul>
              </Accordion.Collapse>
            </Accordion>
          </div>
          <div className="category">
            <div className="product">
              <h3>{categoryTitle}</h3>
              <div className="item-list">
                {this.renderProductList()}
                {this.renderProductList()}
                {this.renderProductList()}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
    productList: state.product.productList,
});

export default connect(
  mapStateToProps,
  { fetchProductByCategory }
)(ListPage);
