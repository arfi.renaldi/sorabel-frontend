import React from 'react';
import { withRouter } from 'react-router';
import { Switch, Route } from 'react-router-dom'

import CategoryPage from './category';
import DetailPage from './detail';
import ListPage from './list';

const App = ({ match }) => {
  return (
    <Switch>
      <Route path='*/category/:category/:categoryList' component={ListPage} />
      <Route path={`${match.path}/category/:category`} component={CategoryPage} />
      <Route path='*/:detail/:id' component={DetailPage} />
    </Switch>
  )
};

export default withRouter(App);
