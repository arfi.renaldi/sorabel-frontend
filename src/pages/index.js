import React from 'react';
import { Container } from 'reactstrap';

import Header from '../components/Layout/Header';
import MainPage from './main';

import './index.css';

const App = () => (
  <React.Fragment>
    <Header />
    <Container>
      <MainPage />
    </Container>
  </React.Fragment>
);

export default App;
