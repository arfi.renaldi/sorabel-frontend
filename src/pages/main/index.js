import React from 'react';
import { withRouter } from 'react-router';
import { Switch, Route, Redirect } from 'react-router-dom'

import ProductPage from '../product';

const Main = () => {
    if (window.location.pathname === '/') {
        return <Redirect to='/products/category/dress'/>
    }

    return (
        <Switch>
            <Route path='/products' component={ProductPage} />
        </Switch>
    )
};

export default withRouter(Main);
