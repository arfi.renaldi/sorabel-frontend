import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'shards-ui/dist/css/shards.min.css';

import App from './pages';
import Root from './Root';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Root>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Root>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
