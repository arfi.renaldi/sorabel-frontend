# Sorabel Front End

## Getting Started

### Prerequisites

* NPM
* Node Js

### How to running
Install packages
```
npm install
```
Run React frontend
```
npm start
```
Run Mock API
```
npm run server
```

## Assumption
Page is started from dress catalog. User can browse dress catalog, I.e. mini dress, midi dress, and maxi dress, product list and product detail. Functional in product detail is dummy. Data retreived from mock API. There is no backend service, testing and CMS.

## Built With

* [Create React App](https://github.com/facebook/create-react-app) - React
* [Redux](https://github.com/reduxjs/react-redux) - React state management

## Authors

* **Arfi Renaldi** - arfirenaldi@gmail.com
